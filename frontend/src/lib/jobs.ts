import type { Directus, ID } from '@directus/sdk';
import type { Job, JobApplication } from 'src/model/job';
import { getDirectusClient, type Collection } from './client';
import { me } from './users';
import { getRequestOptions, mapResult } from './utils';

const directus: Directus<Collection> = await getDirectusClient();

export async function getAllOpenJobs(jwtToken: string): Promise<Job[]> {
    const user = await me(jwtToken);
    const published = 'published';
    const jobs = (
        await directus.items('jobs').readByQuery(
            {
                fields: '*,applications.*',
                filter: {
                    status: {
                        _eq: published,
                    },
                },
                limit: -1
            },
            getRequestOptions(jwtToken)
        )
    ).data;
    return mapResult<Job>(jobs);
}

export async function getJobById(id: ID, jwtToken: string): Promise<Job> {
    const job = await directus.items('jobs').readOne(
        id,
        {
            fields: '*,applications.*'
        },
        getRequestOptions(jwtToken)
    );
    console.log(job);

    return job as Job;
}

export async function getAllJobApplications(jwtToken: string): Promise<JobApplication[]> {
    const user = await me(jwtToken);
    console.log(user.id);

    try {
        const applications = await directus.items('job_applications').readByQuery(
            {
                limit: -1,
                fields: '*,jobs_id.*',
                filter: {
                    users_id: {
                        _eq: user.id!
                    }
                }
            },
            getRequestOptions(jwtToken)
        );
        return (applications.data as JobApplication[])
    } catch (error) {
        console.log(error);
        return [];
    }
}
