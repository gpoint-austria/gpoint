import type { Directus, ID } from '@directus/sdk';
import type { Reward } from 'src/model/reward';
import { getDirectusClient, type Collection } from './client';
import { getRequestOptions, mapResult } from './utils';

const directus: Directus<Collection> = await getDirectusClient();

export async function getAllRewards(jwtToken: string): Promise<Reward[]> {
	const rewards = (
		await directus.items('rewards').readByQuery({ limit: -1 }, getRequestOptions(jwtToken))
	).data;
	return mapResult<Reward>(rewards);
}

export async function getRewardById(id: ID, jwtToken: string): Promise<Reward> {
	const reward = await directus.items('rewards').readOne(id, {}, getRequestOptions(jwtToken));
	return { ...reward };
}
