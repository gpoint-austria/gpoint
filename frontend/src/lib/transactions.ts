import type { Directus, ID } from '@directus/sdk';
import type { Transaction } from 'src/model/transaction';
import { getDirectusClient, type Collection } from './client';
import { me } from './users';
import { getRequestOptions, mapResult } from './utils';

const directus: Directus<Collection> = await getDirectusClient();

export async function getMyTransactions(jwtToken: string): Promise<Transaction[]> {
	const user = await me(jwtToken);

	const transactions = (
		await directus.items('transactions').readByQuery(
			{
				filter: {
					user: {
						_eq: user.id
					}
				},
				limit: -1
			},
			getRequestOptions(jwtToken)
		)
	).data;
	return mapResult<Transaction>(transactions);
}
