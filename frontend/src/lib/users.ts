import type { Directus, ID } from '@directus/sdk';
import { DIRECTUS_PATH, getDirectusClient, type Collection } from './client';
import type { User } from 'src/model/user';
import type { Job, JobApplication } from 'src/model/job';
import { getRequestOptions } from './utils';

const directus: Directus<Collection> = await getDirectusClient();

export async function me(jwtToken: string): Promise<User> {
	const response = await fetch(`${DIRECTUS_PATH}/users/me?access_token=${jwtToken}`);
	return (await response.json()).data;
}

export async function applyToJob(job: Job, jwtToken: string) {
	const myUser = await me(jwtToken);
	const application: JobApplication = {
		jobs_id: job.id!,
		users_id: myUser.id!
	};
	await directus.items('job_applications').createOne(application, {}, getRequestOptions(jwtToken));
}

export async function updateUser(userId: ID, user: User, jwtToken: string) {
	await directus.items('directus_user').updateOne(userId!, user, {}, getRequestOptions(jwtToken));
}
