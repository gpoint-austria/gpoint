import type { Directus, IItems, OneItem, TypeMap } from '@directus/sdk';
import type { Ledger, Transaction } from 'src/model/ledger';
import type { Municipality } from 'src/model/municipality';
import type { User } from 'src/model/user';
import { getDirectusClient, type Collection } from './client';
import { getRequestOptions, mapResult } from './utils';

const directus: Directus<Collection> = await getDirectusClient();

function getTransactionCall(): IItems<Transaction> {
	return directus.items('transactions');
}

function getUserCall(): IItems<User> {
	return directus.items('directus_user');
}

export async function addAmount(user: User, amount: number, jwtToken: string) {
	if (!user.id) {
		console.error('No id given');
		return;
	}
	const dbUser = mapUser(await getUserCall().readOne(user.id, {}, getRequestOptions(jwtToken)));
	const newBalance = !!dbUser.balance ? dbUser.balance + amount : amount;
	await getUserCall().updateOne(
		dbUser.id!,
		{
			balance: newBalance
		},
		{},
		getRequestOptions(jwtToken)
	);
}

export async function subtractAmount(user: User, amount: number, jwtToken: string) {
	if (!user.id) {
		console.error('No id given');
		return;
	}
	const dbUser = mapUser(await getUserCall().readOne(user.id, {}, getRequestOptions(jwtToken)));
	if (dbUser.balance! < amount) {
		console.error('Not enough balance');
		return;
	}
	const newBalance = !!dbUser.balance ? dbUser.balance - amount : amount;
	await getUserCall().updateOne(
		dbUser.id!,
		{
			balance: newBalance
		},
		{},
		getRequestOptions(jwtToken)
	);
}

export async function createTransaction(transaction: Transaction, jwtToken: string) {
	await getTransactionCall().createOne(transaction, {}, getRequestOptions(jwtToken));
}

export async function getLedgerForMunicipality(
	municipality: Municipality,
	jwtToken: string
): Promise<Ledger> {
	const transactionsResult = (
		await getTransactionCall().readByQuery(
			{
				municipality: municipality.id,
				sort: ['date_created']
			},
			getRequestOptions(jwtToken)
		)
	).data;
	const transactions: Transaction[] | undefined = mapResult<Transaction>(transactionsResult);
	return {
		transactions: transactions
	};
}

function mapUser(res: OneItem<User, any> | undefined | null): User {
	return {
		...res
	};
}
