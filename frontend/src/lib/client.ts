import { Directus } from '@directus/sdk';
import type { Job, JobApplication } from 'src/model/job';
import type { Transaction } from 'src/model/ledger';
import type { User } from 'src/model/user';

export type Collection = {
	transactions: Transaction;
	jobs: Job;
	job_applications: JobApplication
	directus_users: User
}

export const DIRECTUS_PATH = 'http://localhost:8055';

async function getDirectusClient() {
	const directus = new Directus<Collection>(DIRECTUS_PATH);
	// if (directus.auth.token) return directus;

	// try {
	// 	if (process.env.DIRECTUS_EMAIL && process.env.DIRECTUS_PASSWORD) {
	// 		await directus.auth.login({
	// 			email: process.env.DIRECTUS_EMAIL,
	// 			password: process.env.DIRECTUS_PASSWORD
	// 		});
	// 	} else if (process.env.DIRECTUS_STATIC_TOKEN) {
	// 		await directus.auth.static(process.env.DIRECTUS_STATIC_TOKEN);
	// 	}
	// } catch (err) {
	// 	if (err.parent.code === 'ECONNREFUSED') {
	// 		console.error(
	// 			'Unable to connect to the Directus instance. Make sure the .env file is present and the VITE_DIRECTUS_URL variable is pointing the correct URL.'
	// 		);
	// 	}
	// }

	return directus;
}

export { getDirectusClient };
