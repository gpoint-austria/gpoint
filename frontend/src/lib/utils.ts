import type { ItemsOptions, OneItem } from '@directus/sdk';
import * as cookie from 'cookie';

export function getRequestOptions(jwtToken: string): ItemsOptions {
	return {
		requestOptions: {
			headers: {
				Authorization: `Bearer ${jwtToken}`
			}
		}
	};
}

export function getJwtToken(request: any) {
	return cookie.parse(request.headers.get('cookie') || '')?.jwt || '';
}

export function mapResult<T>(res: OneItem<T, any>[] | undefined | null): T[] {
	return !!res ? res.filter((a) => !!a).map((b) => ({ ...(b as T) })) : [];
}
