import type { ID } from '@directus/sdk';

export interface Transaction {
	id?: ID;
	title?: string;
	user?: ID;
	municipality?: ID;
	date_created?: string;
}
