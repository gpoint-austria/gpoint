import type { ID } from "@directus/sdk";
import type { Job, JobApplication } from "./job";

export interface User {
    id?: ID;
    balance?: number;
    applied_to?: ID[]
}