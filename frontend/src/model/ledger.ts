import type { ID } from '@directus/sdk';
import type { Municipality } from './municipality';
import type { User } from './user';

export interface Transaction {
	id?: ID;
	user?: ID;
	amount?: number;
	title?: string;
	municipality?: ID;
	date_created?: number;
}

export interface Ledger {
	transactions: Transaction[];
}
