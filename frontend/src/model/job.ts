import type { ID } from "@directus/sdk";
import type { User } from "./user";

export interface Job {
    id?: ID;
    status?: string;
    title?: string;
    description?: string;
    reward?: number;
    application_deadline: string;
    job_start: string;
    job_end: string;
    address: string;
    img: string;
    applications: JobApplication[]
}

export interface JobApplication {
    id?: ID;
    jobs_id: ID;
    users_id: ID;
    status: string;
}