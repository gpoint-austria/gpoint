import type { ID } from "@directus/sdk";

export interface Municipality {
    id?: ID
    post_code?: string;
    name?: string;

}