import type { ID } from '@directus/sdk';

export interface Reward {
	id?: ID;
	status?: string;
	title?: string;
	description?: string;
	price?: number;
	img?: ID;
	municipality?: ID;
}
