import * as cookie from 'cookie';

export async function handle({ event, resolve }) {
	const cookies = cookie.parse(event.request.headers.get('cookie') || '');

	event.locals.jwt = cookies.jwt;
	return await resolve(event);
}

export async function getSession(event) {
	return {
		jwt: event.locals.jwt
	};
}
