import { getJobById } from '$lib/jobs';
import { applyToJob } from '$lib/users';
import { getJwtToken } from '$lib/utils';
import type { Job } from 'src/model/job';

export async function GET({ request, params }) {
	const token = getJwtToken(request);

	try {
		// const job = await getJobById(params.id, token);
		const job = await getJobById(params.id, token);

		console.log(job);
		return {
			body: {
				...job
			}
		};
	} catch (error) {
		return {
			status: 400
		};
	}
}

export async function POST({ request }) {
	const token = getJwtToken(request);

	try {
		const application: Job = await request.json();
		applyToJob(application, token);
		return {
			status: 200
		};
	} catch (error) {
		return {
			status: 500
		};
	}
}
