import { getDirectusClient } from '$lib/client';
import { getAllOpenJobs } from '$lib/jobs';
import { applyToJob } from '$lib/users';
import { getJwtToken } from '$lib/utils';
import type { Job } from 'src/model/job';

const directus = await getDirectusClient();

export async function GET({ request }) {
	const token = getJwtToken(request);

	try {
		const jobs = await getAllOpenJobs(token)
		return {
			body: {
				jobs
			}
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}

