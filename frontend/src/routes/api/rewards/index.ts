import { getAllRewards } from '$lib/rewards';
import { getJwtToken } from '$lib/utils';

export async function GET({ request }) {
	const token = getJwtToken(request);

	try {
		const rewards = await getAllRewards(token);

		return {
			body: {
				rewards
			}
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
