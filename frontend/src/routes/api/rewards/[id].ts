import { getRewardById } from '$lib/rewards';
import { getJwtToken } from '$lib/utils';

export async function GET({ request, params }) {
	const token = getJwtToken(request);

	const { id } = params;

	try {
		const reward = await getRewardById(id, token);
		return {
			body: {
				...reward
			}
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
