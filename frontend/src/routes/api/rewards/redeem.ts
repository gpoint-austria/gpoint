import { createTransaction } from '$lib/point-transfer';
import { getRewardById } from '$lib/rewards';
import { me, updateUser } from '$lib/users';
import { getJwtToken } from '$lib/utils';
import type { Transaction } from 'src/model/ledger';

export async function POST({ request }) {
	const token = getJwtToken(request);

	const payload = await request.json();

	const { rewardId } = payload;

	const myUser = await me(token);

	const reward = await getRewardById(rewardId, token);

	try {
		const transaction: Transaction = {
			user: myUser.id,
			amount: -(reward.price || 0),
			title: reward.title,
			municipality: reward.municipality
		};

		await createTransaction(transaction, token);

		myUser.balance = myUser.balance! - (reward.price || 0);

		await updateUser(myUser.id!, { balance: myUser.balance }, token);
		return {
			status: 201,
			body: {
				status: 'success'
			}
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
