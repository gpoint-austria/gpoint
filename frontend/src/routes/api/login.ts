import { getDirectusClient } from '$lib/client';

export async function POST({ request }) {
	const { email, password } = await request.json();
	const directus = await getDirectusClient();
	const response = await directus.auth.login({
		email,
		password
	});

	return {
		headers: {
			'set-cookie': `jwt=${response.access_token}; Path=/; HttpOnly`
		},
		body: {
			jwt: response.access_token
		}
	};
}
