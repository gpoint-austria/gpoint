import { getMyTransactions } from '$lib/transactions';
import { getJwtToken } from '$lib/utils';

export async function GET({ request }) {
	const token = getJwtToken(request);

	try {
		const transactions = await getMyTransactions(token);
		console.log(transactions);
		return {
			body: transactions
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
