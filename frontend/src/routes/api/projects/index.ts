import { getAllJobApplications } from '$lib/jobs';
import { getJwtToken } from '$lib/utils';

export async function GET({ request }) {
	const token = getJwtToken(request);

	try {
		const jobApplications = await getAllJobApplications(token);

		return {
			body: jobApplications
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
