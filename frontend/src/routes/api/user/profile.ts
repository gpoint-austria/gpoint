import { me } from '$lib/users';
import { getJwtToken } from '$lib/utils';

export async function GET({ request }) {
	const token = getJwtToken(request);

	try {
		const user = await me(token);
		return {
			status: 200,
			body: {
				...user
			}
		};
	} catch (e) {
		return {
			status: e.status
		};
	}
}
